gedit-extra repository
======================

Additional files related to gedit that don't fit in the main repository, to keep
a small size for the main repository.
